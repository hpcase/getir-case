/**
 * Repository of product. This will be injected in init function.
 */
let repo;

/**
 * Return types for response. This will be injected in init function.
 */
let types;


/** init function for inject dependencies */
const init = ({ productRepo, returnTypes }) => {
    repo = productRepo;
    types = returnTypes;
}

/** list action will return of list database function result. */
/**
 * 
 * @param {Start day of filter} startDate  
 * @param {End date of filter} endDate 
 * @param {Maximum sum of counts} maxCount 
 * @param {Minimum sum of counts} minCount 
 */
const list = async ({ startDate, endDate, maxCount, minCount }) => {
    startDate = startDate ? new Date(startDate) : null;
    endDate = endDate ? new Date(endDate) : null;

    //Check if dates are invalid
    startDate = startDate && isNaN(startDate.getTime()) ? null : startDate;
    endDate = endDate && isNaN(endDate.getTime()) ? null : endDate;

    //İf start date greater than endDate, values will swap.
    if (startDate && endDate && startDate > endDate)
        [startDate, endDate] = [endDate, startDate];

    //İf minCount greater than maxCount, values will swap.
    if (minCount != null && maxCount != null && minCount > maxCount)
        [minCount, maxCount] = [maxCount, minCount];

    let result = await repo.list({ startDate, endDate, maxCount, minCount });
    return {
        records: result,
        msg: result && result.length ? "success" : "no-data",
        code: types.SUCCESS_RETURN
    }
}

module.exports = {
    init,
    list
}