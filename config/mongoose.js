/** Environments */
let env = require("../env");

/** Mongoose library */
let mongoose = require("mongoose");

/** Init function prepare mongoose connections. */
var init = async () => {

    /** Creating mongoose connections for each database. With this statement, we can open connection with different databases. */
    let keys = Object.keys(env.database);
    for(let counter = 0; counter < keys.length; counter++){
        mongoose[keys[counter]] = await (mongoose.createConnection(env.database[keys[counter]]));
    }
};

module.exports = {
    init,
    mongoose
};