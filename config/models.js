/** Mongoose library */
const { mongoose } = require("./mongoose");

/** Mongoose model of product table */
const  productModel = require("../models/product");

module.exports = () => {

    /** Inject model dependencies. */
    productModel.init({
        mongoose,
        objectId: mongoose.Types.ObjectId
    })
    
}