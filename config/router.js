/** Router which has routing for product */
const productRouter = require("../routers/product");

/** Node express app */
const app = require("./express");

/** Response data template */
const returnData = require("../utils/returnData");

/** Return codes for response. */
const { ERROR_RETURN, NOT_FOUND_RETURN } = require("../constants/returnTypes")

module.exports = () => {
    /** Init routers */
    app.use('/product', productRouter.router);

    /** 404 Error handling */
    app.use((req, res, next) => {
        res.status(404).send(returnData({
            msg: "not-found",
            code: NOT_FOUND_RETURN
        }));
    });

    /** Server application error handling */
    app.use((err, req, res, next) => {
        res.status(500).send(returnData({
            msg: "error",
            code: ERROR_RETURN
        }));
    });

}