/** Response data template */
const returnTypes = require("../constants/returnTypes");

/** Controller which has product actions */
const productController = require("../controllers/product");

/** Repository which has database functions for product */
const productRepo = require("../repositories/product");


module.exports = () => {

    /** Inject controller dependencies. */
    productController.init({
        productRepo,
        returnTypes
    })
} 
