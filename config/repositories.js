/** Mongoose library */
const { mongoose } = require("./mongoose");

/** Database functoins of product. */
const productRepo = require("../repositories/product");

/** Gives mongoose object of product table. */
const { getProductModel } = require("../models/product");

module.exports = () => {

    /** Inject repository dependencies. */
    productRepo.init({
        productModel: getProductModel(),
        objectId: mongoose.Types.ObjectId,
    });

    /** Checking database for dummy datas. It will create, if database is empty. */
    productRepo.checkDatabase();
}