/** Express library for routing */
const express = require('express')

/** Body parser library for incoming datas */
const bodyParser = require('body-parser')

/** Creating express application */
const app = express();

/** Settings for bodyparser to using of request bodies. */
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(jsonParser);
app.use(urlencodedParser);

/** Content router is reserved for static files. */ 
app.use('/contents', express.static('./contents'));

module.exports = app;