/** Development environment variables */
const development = require("./development.json");

/** Production environment variables */
const production = require("./production.json");

/** Test environment variables */
const test = require("./test.json");

/** Environment file selected by NODE_ENV value. */
module.exports = process.env.NODE_ENV == "test" ? test : 
                    process.env.NODE_ENV == "development" ? development : production;