/**
 * Product mongoose model
 */
let model;

/**
 * Mongoose function which can create Object ID
 */
let objectIdFn;

/** init function for inject dependencies */
const init = ({ productModel, objectId }) => {
    model = productModel;
    objectIdFn = objectId;
}

/** Checking database for dummy datas.We will create, if we dont have. */
const checkDatabase = async () => {
    let count = await countOfDocuments();

    if(count == 0){
        let documents = [];

        for(let counter = 0; counter < 1000; counter++){
            let countsLength = Math.floor((Math.random() * 80) + 20);
            let key = (new objectIdFn()).toString();
            let createdAt = new Date(Date.now() - (Math.floor((Math.random() * 50000000000))))
        
            let counts = [];
            for(let countCounter = 0; countCounter < countsLength; countCounter++){
                counts.push(Math.floor((Math.random() * 9000) + 1000))
            }

            documents.push({
                   key: key, 
                   createdAt: createdAt, 
                   counts: counts
            });
        }

        await bulkInsert(documents);
        return true;
    } else {
        return false;
    }
}

/** list database function */
/**
 * 
 * @param {Start day of filter} startDate  
 * @param {End date of filter} endDate 
 * @param {Maximum sum of counts} maxCount 
 * @param {Minimum sum of counts} minCount 
 */
const list = async ({ startDate, endDate, maxCount, minCount }) => {
    let query = [];

    if (startDate || endDate) {
        let and = { $match: { $and: [] }  };

        startDate ? and.$match.$and.push({ createdAt: { $gte: startDate } }) : "";
        endDate ? and.$match.$and.push({ createdAt: { $lte: endDate } }) : "";

        query.push(and)
    }

    query.push({
        $project: {
            _id: 0,
            key: "$key",
            createdAt: "$createdAt",
            totalCount: {
                $reduce: {
                    input: "$counts",
                    initialValue: 0,
                    in: { $add: ["$$value", "$$this"] },
                }
            }
        }
    });

    if (maxCount != null || minCount != null) {
        let and = { $match: { $and: [] }  };

        maxCount != null ? and.$match.$and.push({ totalCount: { $lte: maxCount } }) : "";
        minCount != null ? and.$match.$and.push({ totalCount: { $gte: minCount } }) : "";

        query.push(and)
    }


    let result = await model.aggregate(query).exec();

    return result;
}

/** Return count of Product collections documents */
const countOfDocuments = async() => {
    return await model.count();
}

/**
 * 
 * @param {array of product document} documents 
 */
const bulkInsert = async(documents) => {
    return await model.insertMany(documents);
}

module.exports = {
    init,
    list,
    countOfDocuments,
    checkDatabase,
    bulkInsert
}