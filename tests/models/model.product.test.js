const chai = require("chai");
const productModel = require("../../models/product");

const mockMongoose = {
    Schema: class Schema {
        constructor() {
        }
    },
    main: {
        model: () => { return "productModel" }
    }
}

const mockobjectId = class ObjectId {
    constructor(brand) {
        this.carname = brand;
    }

    toString() {
        return "ObjectId"
    }
}

productModel.init({
    mongoose: mockMongoose,
    objectId: mockobjectId
})

chai.should();

describe("/model/product.js", async () => {
    let model = productModel.getProductModel();

    it("result should equal mockMongoose.main.model", () => {
        model.should.be.eql(mockMongoose.main.model());
    })
});