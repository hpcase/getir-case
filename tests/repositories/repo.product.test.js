const chai = require("chai");
const productRepo = require("../../repositories/product");

const mockList = [
    {
        "key": "5f3f7ecade999d5387f97750",
        "createdAt": "2020-01-10T07:07:53.659Z",
        "totalCount": 214896
    },
    {
        "key": "5f3f7ecbde999d5387f97762",
        "createdAt": "2019-12-04T03:10:10.163Z",
        "totalCount": 255982
    },
    {
        "key": "5f3f7ecbde999d5387f97764",
        "createdAt": "2020-01-09T00:05:10.905Z",
        "totalCount": 109356
    },
    {
        "key": "5f3f7ecbde999d5387f97774",
        "createdAt": "2019-01-09T00:05:10.905Z",
        "totalCount": 59356
    },
];

const mockModel = {
    aggregate: (query) => {
        return {
            exec: async () => {
                return query.length == 1 ? mockList : [];
            }
        }
    },
    count: async () => {
        return 0
    },
    insertMany: async (arr) => {
        return arr
    },
}

let objectId = class objectIdFn {
    constructor() {
    }

    toString(){
        return "5f3f7ecbde999d5387f97774"
    }
};

    productRepo.init({
        productModel: mockModel,
        objectId
    })

chai.should();

describe("/repositories/product.js/list with no param", async (done) => {
    let result = await productRepo.list({});

    it("result should be array", () => {
        result.should.to.be.an('array');
        result.length.should.to.equal(mockList.length)
    })

    it("result should be array and equal mockList", () => {
        result.should.to.be.an("array")
        result.should.to.eql(mockList)
    })
});

describe("/repositories/product.js/list with param", async () => {
    let result = await productRepo.list({ startDate: new Date(), endDate: new Date(), maxCount: 100, minCount: 5 });

    it("result should be array", () => {
        result.should.to.be.an('array');
        result.length.should.to.equal(0)
    })

    it("msg should be array and equal mockList", () => {
        result.should.to.be.an("array")
        result.should.to.eql([])
    })
});

describe("/repositories/product.js/count", async () => {
    let result = await productRepo.countOfDocuments();

    it("result should be 0", () => {
        result.should.to.be.equal(0)
    })
})

describe("/repositories/product.js/bulkInsert", async () => {
    let result = await productRepo.bulkInsert(mockList);

    it("result should be equal mocklist", () => {
        result.should.to.be.eql(mockList)
    })
})

describe("/repositories/product.js/checkDatabase", async () => {
    let result = await productRepo.checkDatabase();

    it("result should be true", () => {
        result.should.to.be.equal(true)
    })
})