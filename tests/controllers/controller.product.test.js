const chai = require("chai");
const productController = require("../../controllers/product");
const returnTypes = require("../../constants/returnTypes");

const mockList = [
    {
        "key": "5f3f7ecade999d5387f97750",
        "createdAt": "2020-01-10T07:07:53.659Z",
        "totalCount": 214896
    },
    {
        "key": "5f3f7ecbde999d5387f97762",
        "createdAt": "2019-12-04T03:10:10.163Z",
        "totalCount": 255982
    },
    {
        "key": "5f3f7ecbde999d5387f97764",
        "createdAt": "2020-01-09T00:05:10.905Z",
        "totalCount": 109356
    },
    {
        "key": "5f3f7ecbde999d5387f97774",
        "createdAt": "2019-01-09T00:05:10.905Z",
        "totalCount": 59356
    },
];

const repo = {
    list: async ({ startDate, endDate, maxCount, minCount }) => {
        return startDate && endDate && maxCount && minCount ? [] : mockList;
    }
}

productController.init({
    productRepo: repo,
    returnTypes
})

chai.should();

describe("/product/index.js/list with no params", async (done) => {
    let result = await productController.list({});

    it("result should be object", () => {
        result.should.to.be.an('object')
    })

    it("result should have 'records', 'msg', 'code' properties.", () => {
        Object.keys(result).should.to.eql(["records", "msg", "code"])
    })

    it("code should be success", () => {
        result.code.should.to.be.equal(returnTypes.SUCCESS_RETURN)
    })

    it("msg should be success", () => {
        result.msg.should.to.be.equal("success")
    })

    it("msg should be array and equal mockList", () => {
        result.records.should.to.be.an("array")
        result.records.should.to.eql(mockList)
    })
});

describe("/product/index.js/list with params", async (done) => {
    result = await productController.list({ startDate: new Date(), endDate: new Date(), maxCount: 100, minCount: 5 })

    it("result should be object", () => {
        result.should.to.be.an('object')
    })

    it("result should have 'records', 'msg', 'code' properties.", () => {
        Object.keys(result).should.to.eql(["records", "msg", "code"])
    })

    it("code should be success", () => {
        result.code.should.to.be.equal(returnTypes.SUCCESS_RETURN)
    })

    it("msg should be no-data", () => {
        result.msg.should.to.be.equal("no-data")
    })

    it("records should be array and equal []", () => {
        result.records.should.to.be.an("array")
        result.records.length.should.to.equal(0)
    })

});