const chai = require("chai");
const env = require("../../env");
const testEnv = require("../../env/test");

chai.should();

describe("/env/index.js", ()=> {
    it("should be test environment.", () => {
        env.should.to.equal(testEnv);
        env.name.should.to.equal("test");
    })
});