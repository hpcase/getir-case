/** Success response code */
const SUCCESS_RETURN = 0;

/** 404 not found response code */
const NOT_FOUND_RETURN = 1;

/** 500 error response code */
const ERROR_RETURN = 2;

module.exports = {
    SUCCESS_RETURN,
    NOT_FOUND_RETURN,
    ERROR_RETURN
}