/**
 * Mongoose library
 */
let mongo;

/**
 * Mongoose function which can create Object ID
 */
let objectIdFn;

/**
 * Product model will be stored on this variable
 */
let productModel;

/** init function for inject dependencies */
const init = ({ mongoose, objectId }) => {
    mongo = mongoose;
    objectIdFn = objectId;
}

/** Creating product model and return. */
const getProductModel = () => {
    if (!productModel) {
        productModel = new mongo.Schema({
            key: {
                type: String,
                unique: true,
                default: () => {
                    return (new objectIdFn()).toString();
                },
                index: true
            },
            createdAt: {
                type: Date,
                default: () => {
                    return new Date();
                }
            },
            counts: [Number]
        });

        productModel = mongo.main.model('Product', productModel, 'Product');
    }

    return productModel;
}

module.exports = {
    init,
    getProductModel,
}

