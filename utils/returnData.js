/** Response data template */
module.exports = ({
    msg = "", records = null, code = 200
} = {
    msg: "", records: null, code: 200
}) => {
    return {
        msg,
        records,
        code
    }
}