# README #

You can use scripts in package.json to running application.
NODE_ENV have to define as an environment variable. If you don't, application will run with product.json environment.
! For running application, you have to fill development.json or production.json connection string. Application create dummy datas if database is empty. 

You can see [demo server from here](http://hunkar-purtul-case.herokuapp.com/).
You can POST your requests to [this url](http://hunkar-purtul-case.herokuapp.com/product/list)

### Post Body ###

The request payload will include a JSON with 4 fields. These are not required.
    “startDate” and “endDate” fields will contain the date in a “YYYY-MM-DD” format. "createdAt" should be between of “startDate” and “endDate”.
    “minCount” and “maxCount” are for filtering the data. Sum of the “count” array in the documents should be between “minCount” and “maxCount”.

    {
        "startDate": "2016-01-26",
        "endDate": "2018-02-02",
        "minCount": 2700,
        "maxCount": 3000
    }

### Post Response ###

Response payload should have 3 main fields.
“code” is for status of the request. 
“msg” is for description of the code.
“records” will include all the filtered items according to the request.

    {
    "code":0,
    "msg":"Success",
    "records":[
            {
                "key":"TAKwGc6Jr4i8Z487",
                "createdAt":"2017-01-28T01:22:14.398Z",
                "totalCount":2800
            },
            {
                "key":"NAeQ8eX7e5TEg7oH",
                "createdAt":"2017-01-27T08:19:14.135Z",
                "totalCount":2900
            }
        ]
    }
