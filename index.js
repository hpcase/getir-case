//İmports
const env = require("./env");
const express = require('./config/express');
const mongoConfig = require('./config/mongoose');
const modelConfig = require('./config/models');
const repositoryConfig = require('./config/repositories');
const controllerConfig = require('./config/controllers');
const routerConfig = require('./config/router');

const setConfigs = async () => {
  await mongoConfig.init();
  modelConfig();
  repositoryConfig();
  controllerConfig();
  routerConfig();
}

setConfigs();

//Express start to listen
const server = express.listen(process.env.PORT || env.port, () => {
  console.log(`server started on port ${process.env.PORT || env.port} (${env.name})`);
});

//Server close handler
server.on('close', () => {
  process.exit(1);
});

//TODO logging will be inserted
//Server error handler
process.on('unhandledRejection', err => {
  console.log(err)
});


//TODO logging will be inserted
//Server error handler
process.on('uncaughtException', err => {
    console.log(err)
});

module.exports = server;