/** Library for describe async router */
const AsyncRouter = require('express-async-router').AsyncRouter;

/** Controller which has product actions */
const productController = require("../controllers/product");

/** Response data template */
const returnData = require("../utils/returnData");

/**Router object */
const router = AsyncRouter();

/** Product list router with POST action. */
router.post("/list", async (req, res) => {
    let result = await productController.list({
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        maxCount: req.body.maxCount,
        minCount: req.body.minCount
    });

    res.send(returnData(result))
})

module.exports = {
    router
}